# Pixel Art Creator by Alex Tompkins

[Live Demo!](https://crazyal013.gitlab.io/pixel-art-creator/)

This app was built using Vue 3, TypeScript and SCSS.

Also makes use of [Pinia](https://pinia.vuejs.org/), [vue-slider-component](https://www.npmjs.com/package/vue-slider-component), and [vue3-colorpicker.](https://www.npmjs.com/package/vue3-colorpicker)

This app lets you make cool pixel art in various sizes using any colour you like. It has various brush sizes available as well as undo and redo functionality.

## Technical Details

### The Canvas

The canvas for this app is three different Canvas elements each witha  different purpose. 

1. The first canvas is responsible for displaying the 'pixels' that the user has added.
2. The second is used for displaying the hover effect that shows the user which pixel will be filled with colour.
3. The third is the grid canvas, this shows the green overlay grid that divides the canvas up into the 'pixels'.

The canvas is sized to best fit in the available space while also being evenly divisible by the height and width cell count set by the user. This is done so all cells are equal size.

Equal sized cells make it much easier to accurately colour and clear specific cells on the canvas. The grid will also be perfectly situated above the cells with no overflow.