import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'

export const useStore = defineStore('main', {
    state: () => {
        const settings = ref(useLocalStorage('settings', {
            verticalCells: 32 as number,
            horizontalCells: 32 as number,
            gridVisible: true as boolean,
            primaryColour: '#FF0000' as string,
            brushSize: 1 as number,
            maxSize: 128 as number
        }));

        return {
            settings
        }
    },
    actions: {
        toggleGrid() {
            this.$state.settings.gridVisible = !this.$state.settings.gridVisible;
        },
        setVerticalCells(cells: number) {
            this.$state.settings.verticalCells = cells;
        },
        setHorizontalCells(cells: number) {
            this.$state.settings.horizontalCells = cells;
        },
        setCells(cells: number) {
            this.$state.settings.verticalCells = cells;
            this.$state.settings.horizontalCells = cells;
        },
        setPrimaryColour(colour: string) {
            this.$state.settings.primaryColour = colour;
        },
        setBrushSize(size: number) {
            this.$state.settings.brushSize = size;
        }
    },
    getters: {
        isGridVisible(): boolean {
            return this.settings.gridVisible;
        },
        getBrushSize(): number {
            return this.settings.brushSize;
        },
        getVerticalCells(): number {
            return this.settings.verticalCells;
        },
        getHorizontalCells(): number {
            return this.settings.horizontalCells;
        }
    }
});