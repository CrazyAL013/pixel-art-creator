<script setup lang="ts">
import { storeToRefs } from 'pinia';
import { ref, onMounted, watch } from 'vue'
import { useStore } from '../stores/store'
import ControlPanel from './ControlPanel.vue'

const store = useStore();
const { isGridVisible, getHorizontalCells, getVerticalCells } = storeToRefs(store);
const wrapper = ref<HTMLElement>();
const STACK_SIZE: number = 100;

let mainCanvas: HTMLCanvasElement;
let mainContext: CanvasRenderingContext2D;

let gridCanvas: HTMLCanvasElement;
let gridContext: CanvasRenderingContext2D;

let hoverCanvas: HTMLCanvasElement;
let hoverContext: CanvasRenderingContext2D;

let boundingRect: DOMRect;

let cellWidth: number;
let cellHeight: number;

let isDrawing: boolean = false;
let isErasing: boolean = false;

let pixels: string[][] = [];

let undoStack: string[] = [];
let redoStack: string[] = [];

/**
 * Saves the current canvas state to the stack.
 */
function saveState() {
    undoStack.unshift(JSON.stringify(pixels));

    if (undoStack.length > STACK_SIZE) {
        undoStack.pop();
    }
}

/**
 * Performs the undo action.
 */
function undo() {
    if (undoStack && undoStack.length > 0) {
        redoStack.unshift(JSON.stringify(pixels));
        if (redoStack.length > STACK_SIZE) {
            redoStack.pop();
        }

        pixels = [];
        pixels = JSON.parse(undoStack.shift()!);

        fillCells();
        localStorage.setItem("pixels", JSON.stringify(pixels));
    }
}

/**
 * Performs the redo action.
 */
function redo() {
    if (redoStack && redoStack.length > 0) {
        undoStack.unshift(JSON.stringify(pixels));
        if (undoStack.length > STACK_SIZE) {
            undoStack.pop();
        }

        pixels = [];
        pixels = JSON.parse(redoStack.shift()!);

        fillCells();
        localStorage.setItem("pixels", JSON.stringify(pixels));
    }
}

// Watch isGridVisible store value for change.
watch(isGridVisible, () => {
    drawGrid();
});

// Watch cells size store values for change.
watch([getHorizontalCells, getVerticalCells], () => {
    draw();
});

/** 
 * Computes best canvas size given available screen space.
 * 
 * Computes cell size using canvas size.
 */
function draw() {
    // Want width and height to be divisible by respective cell count so all cells are the same pixel size.
    let bestWidth: number = Math.floor(wrapper.value?.offsetWidth! / store.settings.horizontalCells) * store.settings.horizontalCells;
    let bestHeight: number = Math.floor(wrapper.value?.offsetHeight! / store.settings.verticalCells) * store.settings.verticalCells;

    mainCanvas.width = bestWidth;
    mainCanvas.height = bestHeight;

    gridCanvas.width = bestWidth;
    gridCanvas.height = bestHeight;

    hoverCanvas.width = bestWidth;
    hoverCanvas.height = bestHeight;

    // Compute the cell dimensions
    cellWidth = bestWidth / store.settings.horizontalCells;
    cellHeight = bestHeight / store.settings.verticalCells;

    // Get the bounding rectangle of target
    boundingRect = (gridCanvas).getBoundingClientRect();

    fillCells();
    drawGrid();
}

/**
 * Fills the cells of the canvas with colour data.
 * 
 * Also clears old data.
 */
function fillCells() {
    for (let row: number = 0; row < pixels.length; row++) {
        for (let col: number = 0; col < pixels[row].length; col++) {
            if (pixels[row][col] !== '') {
                mainContext.fillStyle = pixels[row][col];
                mainContext.fillRect(row * cellWidth, col * cellHeight, cellWidth, cellHeight);
            } else {
                mainContext.clearRect(row * cellWidth, col * cellHeight, cellWidth, cellHeight);
            }
        }
    }
}

/**
 * Draws the overlay grid.
 * 
 * The grid divides the canvas into visible cells.
 */
function drawGrid() {
    if (isGridVisible.value) {
        gridContext.strokeStyle = '#1cfc03';

        for (let index: number = 0; index < store.settings.horizontalCells; index++) {
            gridContext.beginPath();
            gridContext.moveTo((cellWidth * index), 0);
            gridContext.lineTo((cellWidth * index), gridCanvas.height);
            gridContext.stroke();
        }
        for (let index: number = 0; index < store.settings.verticalCells; index++) {
            gridContext.beginPath();
            gridContext.moveTo(0, (cellHeight * index));
            gridContext.lineTo(gridCanvas.width, (cellHeight * index));
            gridContext.stroke();
        }
    } else {
        gridContext.clearRect(0, 0, gridCanvas.width, gridCanvas.height);
    }
}

/**
 * Check the x,y coordinates are within the grid bounds.
 * 
 * @param x The x coordinate.
 * @param y The y coordinate.
 */
function checkGridBounds(x: number, y: number) {
    if (x >= 0 && x < store.getHorizontalCells && y >= 0 && y < store.getVerticalCells) {
        return true;
    }
    return false;
}

//TODO: merge fillColour and clearColour at some point
/**
 * Fill the selected cell(s) with the currently selected colour.
 *  
 * @param event Mouse click event.
 */
function fillColour(event: MouseEvent) {
    let xIndex: number = Math.floor((event.x - boundingRect.left) / cellWidth);
    let yIndex: number = Math.floor((event.y - boundingRect.top) / cellHeight);

    mainContext.fillStyle = store.settings.primaryColour;

    if (store.getBrushSize > 1) {
        xIndex--;
        yIndex--;
    }
    for (let row: number = 0; row < Math.sqrt(store.getBrushSize); row++) {
        for (let col: number = 0; col < Math.sqrt(store.getBrushSize); col++) {
            if (checkGridBounds((xIndex + row), (yIndex + col))) {
                // Add new colour value to pixels array
                pixels[xIndex + row][yIndex + col] = store.settings.primaryColour;
            }
        }
    }
    mainContext.clearRect(xIndex * cellWidth, yIndex * cellHeight, cellWidth * Math.sqrt(store.getBrushSize), cellHeight * Math.sqrt(store.getBrushSize));
    mainContext.fillRect(xIndex * cellWidth, yIndex * cellHeight, cellWidth * Math.sqrt(store.getBrushSize), cellHeight * Math.sqrt(store.getBrushSize));

    // Save changes to local storage
    localStorage.setItem("pixels", JSON.stringify(pixels));
}

/**
 * Clear the selected cell(s).
 * 
 * @param event Mouse click event.
 */
function clearColour(event: MouseEvent) {
    let xIndex: number = Math.floor((event.x - boundingRect.left) / cellWidth);
    let yIndex: number = Math.floor((event.y - boundingRect.top) / cellHeight);

    if (store.getBrushSize > 1) {
        xIndex--;
        yIndex--;
    }
    for (let row: number = 0; row < Math.sqrt(store.getBrushSize); row++) {
        for (let col: number = 0; col < Math.sqrt(store.getBrushSize); col++) {
            if (checkGridBounds((xIndex + row), (yIndex + col))) {
                // Remove colour value from pixels array
                pixels[xIndex + row][yIndex + col] = '';
            }
        }
    }
    mainContext.clearRect(xIndex * cellWidth, yIndex * cellHeight, cellWidth * Math.sqrt(store.getBrushSize), cellHeight * Math.sqrt(store.getBrushSize));

    // Save changes to local storage
    localStorage.setItem("pixels", JSON.stringify(pixels));
}

/**
 * Get the highlighted colour and set it
 * 
 * @param event Mouse click event.
 */
function copyColour(event: MouseEvent) {
    let xIndex: number = Math.floor((event.x - boundingRect.left) / cellWidth);
    let yIndex: number = Math.floor((event.y - boundingRect.top) / cellHeight);

    store.settings.primaryColour = pixels[xIndex][yIndex];
}

/**
 * Clear the Canvas
 * 
 * Also clears local storage.
 */
function clearCanvas() {
    saveState();
    localStorage.removeItem("pixels");
    pixels = [];
    initializeArray();
    fillCells();
}

/**
 * Draws the grey mouse hover effect on the grid.
 * 
 * @param event Mouse enter event.
 */
function drawHoverEffect(event: MouseEvent) {
    let xIndex: number = Math.floor((event.x - boundingRect.left) / cellWidth);
    let yIndex: number = Math.floor((event.y - boundingRect.top) / cellHeight);

    hoverContext.clearRect(0, 0, gridCanvas.width, gridCanvas.height);
    hoverContext.globalAlpha = 0.2;
    hoverContext.fillStyle = 'grey';
    if (store.getBrushSize > 1) {
        xIndex--;
        yIndex--;
    }
    hoverContext.fillRect(xIndex * cellWidth, yIndex * cellHeight, cellWidth * Math.sqrt(store.getBrushSize), cellHeight * Math.sqrt(store.getBrushSize));
}

/**
 * Sets up the pixels array.
 * 
 * Either empty or with data from local storage.
 */
function initializeArray() {
    if (localStorage.getItem("pixels") === null) {
        for (let x: number = 0; x < store.settings.maxSize; x++) {
            pixels.push([]);
            for (let y: number = 0; y < store.settings.maxSize; y++) {
                pixels[x][y] = '';
            }
        }
    } else {
        pixels = JSON.parse(localStorage.getItem("pixels")!);
    }
}

/**
 * Watch for page resize and redraw the canvas.
 */
const resizeOberserver: ResizeObserver = new ResizeObserver(entries => {
    entries.forEach(entry => {
        draw();
    });
});

onMounted(() => {
    initializeArray();
    mainCanvas = <HTMLCanvasElement>document.getElementById('canvas');
    gridCanvas = <HTMLCanvasElement>document.getElementById('grid-canvas');
    hoverCanvas = <HTMLCanvasElement>document.getElementById('hover-canvas');

    if (mainCanvas.getContext && gridCanvas.getContext && hoverCanvas.getContext) {
        mainContext = mainCanvas.getContext('2d')!;
        gridContext = gridCanvas.getContext('2d')!;
        hoverContext = hoverCanvas.getContext('2d')!;

        resizeOberserver.observe(wrapper.value!);
        gridCanvas.addEventListener('click', function (event) {
            if (event.ctrlKey) {
                // copy hovered colour
                copyColour(event);
            } else {
                fillColour(event);
            }
        });
        gridCanvas.addEventListener('contextmenu', function (event) {
            event.preventDefault();
            clearColour(event);
        });

        gridCanvas.addEventListener('mousedown', function (event) {
            saveState();
            if (event.button === 0  && !event.ctrlKey) {
                isDrawing = true;
            } else if (event.button === 2) {
                isErasing = true;
            }
        });
        gridCanvas.addEventListener('mouseup', function (event) {
            if (isDrawing && !event.ctrlKey) {
                isDrawing = false;
            } else if (isErasing) {
                isErasing = false;
            }
        });
        gridCanvas.addEventListener('mousemove', function (event) {
            if (isDrawing) {
                fillColour(event);
            } else if (isErasing) {
                clearColour(event);
            }
            drawHoverEffect(event);
        });
        gridCanvas.addEventListener('mouseleave', function (event) {
            isDrawing = false;
            isErasing = false;
            hoverContext.clearRect(0, 0, gridCanvas.width, gridCanvas.height);
        });
        document.addEventListener('keydown', (event) => {
            if (event.ctrlKey && event.key === 'z') {
                undo();
            }
        });
        document.addEventListener('keydown', (event) => {
            if (event.ctrlKey && event.key === 'y') {
                redo();
            }
        });

        draw();
    }
});
</script>

<template>
    <div class="menu">
        <ControlPanel @clearCanvas="clearCanvas()" />
    </div>
    <div class="canvas">
        <div class="wrapper" ref="wrapper">
            <canvas class="main-canvas" id="canvas" style="z-index: 1;"></canvas>
            <canvas id="hover-canvas" style="z-index: 2;"></canvas>
            <canvas id="grid-canvas" style="z-index: 3;"></canvas>
        </div>
    </div>
</template>

<style lang="scss" scoped>
.canvas {
    padding: 10px;
    .wrapper {
        width: 100%;
        height: 100%;
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;

        canvas {
            position: absolute;
            background-color: transparent;
        }

        .main-canvas {
            background-color: white;
        }
    }
}
</style>
